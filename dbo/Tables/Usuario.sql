﻿CREATE TABLE [dbo].[Usuario] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Correo]     VARCHAR (50)   NOT NULL,
    [Contrasena] VARBINARY (50) NOT NULL,
    [IdPersona]  INT            NOT NULL,
    CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Usuario_Persona] FOREIGN KEY ([IdPersona]) REFERENCES [dbo].[Persona] ([Id])
);

