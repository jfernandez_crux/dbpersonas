﻿CREATE TABLE [dbo].[Persona] (
    [Id]              INT          IDENTITY (1, 1) NOT NULL,
    [Cedula]          VARCHAR (15) NOT NULL,
    [Nombre]          VARCHAR (50) NOT NULL,
    [ApellidoUno]     VARCHAR (50) NOT NULL,
    [ApellidoDos]     VARCHAR (50) NOT NULL,
    [FechaNacimiento] DATETIME     NOT NULL,
    CONSTRAINT [PK_Persona] PRIMARY KEY CLUSTERED ([Id] ASC)
);

