﻿CREATE PROCEDURE [dbo].[InsertarPersonas] (@Cedula VARCHAR(15), @Nombre VARCHAR(50), @ApellidoUno VARCHAR(50), @ApellidoDos VARCHAR(50), @FechaNacimiento DATETIME)
AS
INSERT INTO [dbo].[Persona]
           ([Cedula]
           ,[Nombre]
           ,[ApellidoUno]
           ,[ApellidoDos]
           ,[FechaNacimiento])
     VALUES
           (@Cedula
           ,@Nombre
           ,@ApellidoUno
           ,@ApellidoDos
           ,@FechaNacimiento)