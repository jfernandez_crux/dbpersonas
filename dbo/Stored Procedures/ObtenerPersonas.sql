﻿CREATE PROCEDURE ObtenerPersonas (@Id INT NULL, @Cedula INT NULL)
AS
	IF @Id is not null
	BEGIN
		SELECT [Id]
			  ,[Cedula]
			  ,[Nombre]
			  ,[ApellidoUno]
			  ,[ApellidoDos]
			  ,[FechaNacimiento]
		  FROM [dbo].[Persona]
		  WHERE [Persona].[Id] = @Id
	END
	ELSE IF @Cedula is not null
	BEGIN
		SELECT [Id]
			  ,[Cedula]
			  ,[Nombre]
			  ,[ApellidoUno]
			  ,[ApellidoDos]
			  ,[FechaNacimiento]
		  FROM [dbo].[Persona]
		  WHERE [Persona].[Cedula] = @Cedula
	END
	ELSE
	BEGIN
		SELECT [Id]
			  ,[Cedula]
			  ,[Nombre]
			  ,[ApellidoUno]
			  ,[ApellidoDos]
			  ,[FechaNacimiento]
		  FROM [dbo].[Persona]
	END